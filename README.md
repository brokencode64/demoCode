## Summary
This project contains demonstration code with two sections. The first is a kubernetes repository and the second is a prometheus-exporter written in python. The Kubernetes section will deploy a K3D-based kubernetes cluster, install argoCD with helm then template an "app-of-apps pattern" which will allow argoCD to manage itself. Lastly it will then deploy a demo set of microservices via the same "app-of-apps" pattern using ArgoCD and Helm. Both the ArgoCD GUI and microservices' website will be available to the localhost via kubernetes ingress. The Prometheus exporter will expose various air-quality metrics for specified regions via the OpenWeatherMap library.

Once deployed the following should be available by web (on local host):
* ArgoCD GUI - argo.localhost:8080
* Robot shop GUI - robot.localhost:8080


## Pre-reqs

The following need to be installed before standing up a cluster:
* Unix/Linux OS
* Kubectl
* K3D
* Docker
* helm cli

Optional:
* kubectl krew plugin manager
* kubectl Kubens plugin
* kubectl Kubectx plugin
* Kubectl view-secret plugin
* Helm cm-push plugin


## Process overview
1. Create kubernetes cluster with K3D
2. Install ArgoCD with helm
3. Setup "App-of-Apps" to Allow ArgoCD to manage itself
4. Use ArgoCD to deploy robot-shop application


## Kubernetes Manifests Explanation
- apps
  - Templates
    - root.yaml
     * Root application definition
     * Generates manifests for other applications
    - argo-cd.yaml
      - App manifest allowing ArgoCD to manage itself
      - Will be used by root app
    - robot-shop.yaml
      - App manifest that will create the robot-shop application
      - Will be used by root app
  - Chart.yaml
    * Root app template
  - Values.yaml
    * Root app template  
    * Left empty intentionally
- charts
  - argo-cd
    * Helm charts for argocd
- Ingress
  - ingress.yaml
    * Creates an ingress for robot-shop deployment
    * Ideally this would be part of the helm-chart and specified in 'values.yaml'


## List of microservices in robot-shop
* Cart service
* Catalogue service
* Dispatch service
* Mongodb Service
* Mysql Service
* Payment Service
* Rabbitmq service
* Ratings Service
* Redis service
* Shipping service
* Web Service


## K3D

**Installing K3D**

Installing via bash script:
```
# Get the latest install script 
wget -q -O - https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh 

# Run the script
bash install.sh
```
Manual installation:
```
## Get the appropriate ``build from github:
wget https://github.com/k3d-io/k3d/releases/download/v5.3.0/k3d-linux-amd64

## Move to someplace in PATH:
mv k3d-linux-amd64 ~/.local/bin/k3d

## Add custom path to .bashrc if necessary:
export PATH="$HOME/bin:$PATH"
```
Verify K3D cli is available:
```
k3d --version
```

**Setting Up Cluster**

Single Node Cluster:
```
# Create basic cluster with ports 8080 and 8443 exposed and mapeed to 80 and 443 on the loadbalancer:
k3d cluster create [cluster-name] -p "8080:80@loadbalancer" -p "8443:443@loadbalancer"


# Add a new node:
k3d node create [newserver name] --cluster multiserver --role server

Multi-node with ETCD:
# Create three node-cluster with ports 8080 and 8443 exposed and mapped to 81 and 443 on the loadbalancer:
k3d cluster create [cluster name] -p "8080:80@loadbalancer" -p "8443:443@loadbalancer" --servers 3
```

Verify Cluster is running:
```
# Get cluster info:
kubectl cluster-info

# Verify node status:
kubectl get nodes

# Verify kube-system status:
kubectl get pods -n kube-system

# Verify docker containers are running:
docker ps
```

## Kubernetes
**Setting up ArgoCD**

Install ArgoCD chart:
```
# Add argocd helm update
helm repo add argo-cd https://argoproj.github.io/argo-helm

# Generate chart-lock and update helm dependencies
cd k8s
helm dep update charts/argo-cd

# Initialy install argocd via helm
helm install argo-cd charts/argo-cd/ --create-namespace --namespace argo-cd
```

Create root application and child applications:
```
# template root application and apply
helm template apps/ | kubectl apply -f -

# Delete helm argocd secret as ArgoCD can now manage itself
kubectl delete secret -l owner=helm,name=argo-cd
```

Once the child applications are running you can create the ingress for robot-shop:
```
# Apply the ingress object
kubectl apply -f ingress/robot_ingress.yaml
```

At this point the Argocd and robot-applications should be created and reachable via web brower from the localhost.


## Python

A simple air quality index exporter for Prometheus written in python. You will need add an api key to the script before running or configure it to use an environment variable and export the api key as an environment variable.
```

# Run exporter
python main.py

# Check metrics
curl localhost:9091
````````

## Diagram of K3D setup and functionality
```
                                                                                       ┌──────────────────────────────────────────────┐
                                                                                       │            Application Helm Repository       │
                                                                                       ├──────────────────────────────────────────────┤
                                                                                       │                                              │                                                                  ┌──────────────────────────────────────────────┐
                                                                                       │    ┌───────────────┐     ┌───────────────┐   │                                                                  │               Web Applications               │
                                                                                       │    │Robot App Repo │     │  ArgoCD Repo  │   │                                                                  ├──────────────────────────────────────────────┤
                                                                                       │    └───────────────┘     └───────────────┘   │                                                                  │                                              │
                                                                                       │                                              │                                                                  │ ┌───────────────────┐  ┌───────────────────┐ │
                                                                                       └────────────────────────┬─────────────────────┘                                                                  │ │  Robot WebStore   │  │     ArgoCD GUI    │ │
                                                                                                          ▲     │                                                                                        │ └───────────────────┘  └───────────────────┘ │
                                                                                                          │     │                                                                                        │                                              │
                                                                                                          │     │                                                                                        │                                              │
                                                                                                          │     │                                                                                        └──────────────────────────────────────────────┘
                                                                                                          │     │Pull helm chart from repo                                                                                       ▲
                                                                                                          │     │                                                                                                                │
┌──────────────────────────────────────────────┐                                                          │     │                                                                                                                │
│                                              │                                                          │     │                                                                                                                │
│              Code Commit by user             │                                  ┌───────────────────────┼─────┼──────────────┬────────────────────────────────────────────────────────────────────────────┬────────────────────┼─────────────────────────┐
│                                              │                                  │                       │     │              │                               K3D K8s Cluster                              │                    │                         │
└──────────────────────┬───────────────────────┘                                  │                       │     │              └────────────────────────────────────────────────────────────────────────────┘                    │                         │
                       │                                                          │                       │     │                                                                                                                │                         │
                       │                                                          │                       │     │                                                                                        ┌───────────────────────┼─────────────────────┐   │
                       │                                                          │                       │     │                                                                                        │                       │                     │   │
                       │                                                          │                       │     ▼                                                                                        │  ┌────────────────────┴───────────────────┐ │   │
                       │                                                          │   ┌───────────────────┴──────────────────────────┐                                                                   │  │         Traeffik Load-Balancer         │ │   │
                       │                                                          │   │                    ArgoCD                    │                                                                   │  └────────────────────────────────────────┘ │   │
                       ▼                                                          │   ├──────────────────────────────────────────────┤                                                                   │                       ▲                     │   │
┌──────────────────────────────────────────────┐                                  │   │                                              │                                                                   │                       │                     │   │
│                 Gitlab Repo                  │                                  │   │             ┌──────────────────┐             │                                                                   │  ┌────────────────────┴───────────────────┐ │   │
├──────────────────────────────────────────────┤                                  │   │             │     Root App     │             │  Generate new Helm release from template and apply with kubectl   │  │           Traeffik Ingresses           │ │   │
│                                              │         Poll for changes         │   │             └────────┬─────────┘             ├──────────────────────────────────────────────────────────────────►│  └────────────────────────────────────────┘ │   │
│          ┌──────────────────────┐            │◄─────────────────────────────────┼───┤                      │                       │                                                                   │                       ▲                     │   │
│          │       K8s/Apps/*     │            │                                  │   │                      ▼                       │             Sync application Status and verify health             │                       │                     │   │
│          └──────────────────────┘            ├──────────────────────────────────┼──►│       ┌───────────────────────────────┐      │◄──────────────────────────────────────────────────────────────────┤  ┌────────────────────┴───────────────────┐ │   │
│                                              │               Sync               │   │       │  Generate apps from template  │      │                                                                   │  │              K8s Service               │ │   │
└──────────────────────────────────────────────┘                                  │   │       ├───────────────────────────────┤      │              Apply any needed changes if out-of-sync              │  └────────────────────────────────────────┘ │   │
                                                                                  │   │       │                               │      ├──────────────────────────────────────────────────────────────────►│                       ▲                     │   │
                                                                                  │   │       ▼                               ▼      │                                                                   │                       │                     │   │
                                                                                  │   │   ┌──────────────────┐ ┌──────────────────┐  │                                                                   │  ┌────────────────────┴───────────────────┐ │   │
                                                                                  │   │   │     Robot App    │ │    ArgoCD APP    │  │                                                                   │  │              K8s Deployment            │ │   │
                                                                                  │   │   └──────────────────┘ └──────────────────┘  │                                                                   │  └────────────────────────────────────────┘ │   │
                                                                                  │   │                                              │                                                                   │                       ▲                     │   │
                                                                                  │   └──────────────────────────────────────────────┘                                                                   │                       │                     │   │
                                                                                  │                                                                                                                      │  ┌────────────────────┴───────────────────┐ │   │
                                                                                  │                                                                                                                      │  │     Helm Application Installation      │ │   │
                                                                                  │                                                                                                                      │  └────────────────────────────────────────┘ │   │
                                                                                  │                                                                                                                      │                                             │   │
                                                                                  │                                                                                                                      └─────────────────────────────────────────────┘   │
                                                                                  │                                                                                                                                                                        │
                                                                                  │                                                                                                                                                                        │
                                                                                  └────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```
