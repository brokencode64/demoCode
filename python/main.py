from pyowm import OWM
from prometheus_client import start_http_server, Gauge


# Locations to get data for
LOCATIONS = [
    {'city': 'Amsterdam', 'country': 'NL'},
    {'city': 'Utrecht', 'country' : 'NL'},
]


#  Dictionary of Prometheus metrics to be exposed
PROM_METRICS = {
    key: Gauge(key, "Metric for {key}", ['city', 'id'])
    for key in ['aqi', 'co', 'no', 'no2', 'o3', 'so2', 'pm2_5', 'pm10', 'nh3']
}


# Get aqi
def get_air_quality():
    # Open Weather Map Client
    owm = OWM('')
    mgr = owm.airpollution_manager()
    # Iterate through all locations in our list
    for location in LOCATIONS:
        try:
            # Get location city and country
            city = location["city"]
            country = location["country"]

            # Create registry with location details
            reg = owm.city_id_registry()
            location_details = reg.locations_for('{}'.format(city), country='{}'.format(country), matching='exact')
            current_city = location_details[0]
            city_id = current_city.id
            city_name = current_city.name
            lat = current_city.lat
            lon = current_city.lon

            # Get overall air status
            air_status = mgr.air_quality_at_coords(lat, lon)

            # Set vaules for each Prometheus Gauge Metric in Dictionary
            for key, metric in PROM_METRICS.items():
                metric.labels(city=city_name, id=city_id).set(getattr(air_status, key))

        except Exception as err:
            print('Error:', err)


# Get current air quality
def get_metrics():
    get_air_quality()


# Start metrics server and expose metrics
if __name__ == '__main__':
    start_http_server(9091)

    while True:
        get_metrics()
